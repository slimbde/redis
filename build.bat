@echo off

docker stop redis
docker build -t slimbde/redis .
docker run --rm -d -p 6379:6379 --net test-network --name redis -v "C:/SPA/docker/redis/redis.conf":/redis.conf slimbde/redis --requirepass "redis"