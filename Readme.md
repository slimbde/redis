# REDIS

https://youtu.be/jgpVdJB2sKQ

To be able to connect one container to another you should create a network

```ps
docker network create test-network
```

Then buid your container and start it

```ps
docker run --rm -d -p 6379:6379 --net test-network --name redis -v "C:/SPA/docker/redis/redis.conf":/redis.conf slimbde/redis --requirepass "redis"
```

Now you can connect to that instance via `test-network` from another container using `-h` flag (the host)

```ps
docker run -it --rm --net test-network slimbde/redis redis-cli -h redis --pass redis
```
